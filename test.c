#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>
#include <time.h>

#include "arduino-serial-lib.h"

float calculateSD(float data[]);

void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	int fd = -1;
	int baudrate = 9600;  // default
	int humidity = 0;
	int temperature = 0;
  float minhum = 999;
  float mintemp = 999;
  float maxhum = -1;
  float maxtemp = -1;
	float temperaturas [12];
	float humedads [12];
  float sumTemp = 0.0;
  float sumHum = 0.0;
	char temp = 't';
	char hum = 'h';
	int tomadas = 0;
	fd = serialport_init("/dev/ttyACM0", baudrate);

	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;
	}

	serialport_flush(fd);

	while(tomadas<12){
		write(fd, &temp, 1);
		usleep(5000);
		read(fd, &temperature, 1);
		printf("%s %d \n", "Temperatura:" ,temperature);

		temperaturas[tomadas] = (float) temperature;

    if ((float)  temperature > maxtemp){
      maxtemp = (float) temperature;
    }

    if ((float)  temperature < mintemp){
      mintemp = (float) temperature;
    }

    sumTemp+=(float) temperature;

		write(fd, &hum, 1);
		usleep(5000);
		read(fd, &humidity, 1);
		printf("%s %d \n", "Humedad:" , humidity);
		humedads[tomadas] = (float) humidity;

    if ((float)  humidity > maxhum){
      maxhum = (float) humidity;
    }

    if ((float)  humidity < minhum){
      minhum = (float) humidity;
    }
    sumHum+= (float) humidity;

		sleep(5);

		tomadas+=1;

	}

	close( fd );
	printf("%s %f \n", "El valor máximo de TEMPERATURA es: " , maxtemp);
	printf("%s %f \n", "El valor mínimo de TEMPERATURA es: " , mintemp);
  printf("%s %f \n", "La desviación estándar para las temperaturas es: ", calculateSD(temperaturas) );
  printf("%s %f\n", "La media para las temperaturas es: ", sumTemp/tomadas);

	printf("%s %f \n", "El valor máximo de HUMEDAD es: " , maxhum);
	printf("%s %f \n", "El valor mínimo de HUMEDAD es: " , minhum);
  printf("%s %f\n", "La desviación estándar para la humedad es: ", calculateSD(humedads) );
  printf("%s %f\n", "La media para la humedad es: ",sumHum/tomadas);


	return 0;
}
/*
float valmin(float data[]){
	float min = 999.0;

	for(int i = 0; i < sizeof(data)/sizeof(data[0]); ++i)
    {
        if(data[i]< min){
        	min = data[i];
        }
    }

    return min;

}

float valmax(float data[]){
	float max = -1.0;

	for(int i = 0; i < sizeof(data)/sizeof(data[0]); ++i)
    {
        if(data[i]> max){
        	max = data[i];
        }
    }

    return max;

}*/


/* Ejemplo para calcular desviacion estandar*/
float calculateSD(float data[])
{
    int sizeData = sizeof(data)/sizeof(data[0]);
    float sum = 0.0, mean, standardDeviation = 0.0;

    for(int i = 0; i < sizeData ; ++i)
    {
        sum += data[i];
    }



    mean= sum/sizeData;
    for(int i = 0; i < sizeData; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation / sizeData);
}

/*
float calculateMean(float data[]){

  int sizeData = sizeof(data)/sizeof(data[0]);
  float mean;
  float sum = 0.0;
  int i;
  for(i = 0; i < sizeData ; ++i)
  {
      sum += data[i];
  }

  mean = sum/sizeData;
  return mean;
}
*/
